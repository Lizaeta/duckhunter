using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class TargetCollider : DefaultObserverEventHandler
{
    public static TargetCollider instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        moveTarget();
    }
    public void moveTarget()
    {
        Vector3 temp;
        temp.x = Random.Range(-30f, 30f);
        temp.y = Random.Range(6f, 15f);
        temp.z = Random.Range(-30f, 30f);
        transform.position = new Vector3(temp.x, temp.y, temp.z);
        if (DefaultObserverEventHandler.trueFalse == true)
        {
            RaycastController.instance.playSound(0);
        }


    }

}
