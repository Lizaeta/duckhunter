using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdController : MonoBehaviour
{
    private Transform TargetFocus;

    void Start()
    {
        TargetFocus = GameObject.FindGameObjectWithTag("Target").transform;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 target = TargetFocus.position - this.transform.position;
        Debug.Log(target.magnitude);

        if (target.magnitude < 1)
        {
            TargetCollider.instance.moveTarget();
        }


        transform.LookAt(TargetFocus.transform);
        float speed = Random.Range(15f, 30f);
        transform.Translate(0, 0, speed * Time.deltaTime);
    }
}
